package com.uz.installzoo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UserExcistException extends RuntimeException {
    public UserExcistException(String message) {
        super(message);
    }
}
